# Webseite

Das ist das Repository der Webseite von Freifunk Holzwinkel.

Es wird der Static Site Generator [Hugo](https://gohugo.io/) verwendet.

Zum Testen der Webseite: `hugo serve`
