require(['./src/remark/api'], function(Api){
    window.remark = new Api();
    require(['./src/polyfills'], function(polyfills){
    polyfills.apply();
    require(['./src/remark/components/styler/styler'], function(styler){
        styler.styleDocument();
    });
});
});

// Expose API as `remark`


// Apply polyfills as needed


// Apply embedded styles to document

