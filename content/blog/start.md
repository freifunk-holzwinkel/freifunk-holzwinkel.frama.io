---
title: "Die Anfänge von Freifunk Holzwinkel"
date: 2018-08-31T20:35:47+06:00
type: post
image: freifunk.png
author: 
tags: []
---
# Die entscheidende Entscheidung
Heute Abend habe ich beschlossen, in Welden und Umgebung eine lokale Freifunk-Gruppe zu gründen und mit dieser ein freies, flächendeckendes, dezentrales, unzensiertes und anonymes WLAN-Mesh-Netzwerk aufzubauen.
