---
title: "WhatsApp <-> Matrix Bridge"
date: 2019-10-09T22:14:21+02:00
draft: true
tags: []
---
# WhatsApp oder Matrix?

Beides! WhatsApp hat die Nutzer, Matrix hat die Freiheit. Deswegen haben wir uns für beide Messenger entschieden. Dank einer Bridge werden die Nachrichten automatisch weitergeleitet.

## Hintergrund
Um mit anderen Freifunkern in der Community in Kontakt zu bleiben und das Projekt zu koordinieren, empfiehlt sich ein Instant Messenger. Wegen seiner Offenheit und Dezentralität gibt es schon seit den Anfangstagen von Freifunk Holzwinkel eine Matrix-Gruppe unter [#freifunk-holzwinkel:tchncs.de](https://matrix.to/#/#freifunk-holzwinkel:tchncs.de).
<br>
Wenn jemand Interesse an Freifunk Holzwinkel bekundet, wäre ein logischer Schritt, den Interessenten zum Chat hinzuzufügen, damit man weiter in Kontakt bleiben kann. "Hast du Matrix?" - "Nein, was ist das? Hat das was mit dem Film zu tun?" - "Matrix ist eine alternative zu WhatsApp und funktioniert ähnlich wie E-Mail. Willst du dir die App installieren?"
Für die meisten Menschen ist hier eine abschreckende Hürde erreicht: "Wieso nur für Freifunk eine extra App installieren? Dann doch lieber nicht...".
Ab sofort könnte der Dialog so ablaufen: "Hast du WhatsApp?" - "Ja klar" (WhatsApp hat in Deutschland einen Marktanteil von _____ % [QQUUEELLEE]()) - "Soll ich dich zur Gruppe hinzufügen?" - "Ja, gerne. Hier hast du meine Nummer: _____".
Dank der Bridge ist die Gruppe nicht auf WhatsApp beschränkt, sondern Freiheits- und Privatsphäreorientierte Nutzer können weiterhin Matrix nutzen, sich aber trotzdem mit allen austauschen.

## Bridge-Software
Als Software-Basis dient die in Go???? implementierte [Matterbridge](https://github.com/42wim/matterbridge). Neben dem Namensgeber Mattermost beherrscht die Bridge das für Freifunk Holzwinkel benötigte Matrix und WhatsApp, darüber hinaus sogar noch 16 weitere Messenger wie Telegram, Slack, Discord, IRC und XMPP.

Die Einrichtung ist zügig erledigt: In der Konfigurationsdatei werden die nötigen Informationen wie Zugangsdaten und zu bridgende Gruppen eingetragen [(weitere Infomrationen zur Konfiguration im Matterbridge-Wiki)](https://github.com/42wim/matterbridge/wiki/How-to-create-your-config) und schon könnte man die Bridge mit dem Aufruf "mattermost" starten.

## Sonderfall WhatsApp
Wäre da nicht WhatsApp. Da WhatsApp seine einzigste offizielle API, die [(Business) API](https://www.whatsapp.com/business/api), nur größeren Unternehmen zur Verfügung stellt und bei Verwendung von Dritt-Clients den Zugang sperrt ([Quelle](https://v-i-t-t-i.de/2014/11/whatsapp-sperrt-nutzer-wegen-nicht-autorisierter-nutzung-durch-dritt-clients-38631/) [Quelle](https://www.cnet.de/88143883/whatsapp-stoppt-beliebten-dritt-client-whatsapp-plus/) [Quelle](https://stadt-bremerhaven.de/whatsapp-sperrt-wieder-dritt-clients/) [Quelle](https://github.com/tgalal/yowsup/issues/2829#issuecomment-506225935) [Quelle](https://github.com/venomous0x/WhatsAPI)), kommt eine direkte Verbindung mit den WhatsApp-Servern nicht infrage.

Matterbridge geht den Umweg über die Schnittstelle für [WhatsApp Web](https://web.whatsapp.com/).
Mit WhatsApp Web kann man den Messenger in einem Browser-Fenster am Desktop-Computer verwenden und so Nachrichten versenden und mit WhatsApp interagieren, wie man es auch direkt in der App machen kann. Jedoch ist dies kein eigenständiger Client, sondern benötigt eine ständige Verbindung zur laufenden WhatsApp-App.

Deswegen muss parallel zur Bridge auch eine WhatsApp-Installation betrieben werden, die dauerhaft online ist.
Um die WhatsApp-App dauerhaft zu betreiben, hat man mehrere Möglichkeiten. Ein ausrangiertes Android-Smartphone könnte dafür herhalten oder man versucht sein Glück mit Android (z.B. mit [Emteria OS](https://emteria.com/)) auf einem [Raspberry Pi](https://www.raspberrypi.org/). Letzteres hat bei mir nicht geklappt, da die offizielle Raspberry Pi-Kamera beim QR-Code-basierten Verbindungsprozess über die WhatsApp Web-Schnittstelle eingefroren ist.

## Android-Emulator
Eine weitere und die für meine Zwecke beste Möglichkeit ist, WhatsApp innerhalb des [offiziellen Android-Emulators](https://developer.android.com/studio/run/emulator) auf einem normalen Computer zu betrieben. Das hat den Vorteil, dass man den Emulator nach dem Einrichten für eine höhere Verfügbarkeit, und damit man seinen eigenen Computer nicht durchlaufen lassen muss, auf einem Server im Rechenzentrum betreiben kann.

Wenn man die [Kommandozeilentools](https://developer.android.com/studio/#command-tools) verwendet, muss man nicht das komplette Android Studio-Paket auf seinem Computer installieren. Mithilfe des Tools [sdkmanager](https://developer.android.com/studio/command-line/sdkmanager) installiert man die nötigen Android SDK-Pakete, mit dem [avdmanager](https://developer.android.com/studio/command-line/avdmanager) erstellt man ein virtuelles Gerät. Schließlich kann man mit [emulator](https://developer.android.com/studio/run/emulator-commandline) dem Emulator starten.

Der [GitHub Wiki-Eintrag](https://github.com/tulir/mautrix-whatsapp/wiki/Android-VM-Setup) eines anderen WhatsApp-Bridge Projektes beschreibt auch die Einrichtung eines virtuellen Android-Gerätes. Darüber hinaus wird dort erklärt, wie man mit einer virtuellen Kamera den QR-Code-basierten Verbindungsprozess für WhatsApp Web durchlaufen kann.


## Server
Der Server, auf dem die Bridge betrieben wird, ist ein vServer mit 2 vCores und 4 GB Ram bei dem Hostinganbieter Netcup.
Server: 
Netcup: https://www.netcup.de
kleiner Server: RAM ausreichend, aber bei Start dauerhaft auf 100 %: https://www.netcup.de/vserver/vps.php#v-server-details
=> auf größeren umgestiegen

* automatisch neustarten
* rsync --no-whole-file  -e ssh -avz -v --stats ffhw-bridge.avd.funktioniert/ user@185.233.105.212:.android/avd/ffhw-bridge.avd.funktioniert2/
* tmux
* emulator -dns-server 9.9.9.9 -no-audio -no-window -no-accel -avd ffhw-bridge
* adb shell
  am start -n "com.whatsapp/.Main"
  monkey -p ${package_name} -c android.intent.category.LAUNCHER 1;
* modprobe v4l2loopback
  ffmpeg -f x11grab -s 640x480 -i :0.0+10,20 -vf format=pix_fmts=yuv420p -f v4l2 /dev/video0
* sdkmanager --list
sdkmanager --install "platforms;android-28"
sdkmanager --install "system-images;android-28;default;x86_64"
avdmanager create avd -n whatsapp-bridge -k "system-images;android-28;default;x86_64"
avdmanager list avd
emulator -avd whatsapp-bridge
https://stackoverflow.com/questions/39645178/panic-broken-avd-system-path-check-your-android-sdk-root-value
