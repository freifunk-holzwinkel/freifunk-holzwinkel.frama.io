---
title: "Infos"
date: 2019-03-11T20:35:47+06:00
author:
tags: []
---
# Infos
* [Router aufstellen](/router-aufstellen)
* [Wie kann ich mitmachen bei Freifunk Holzwinkel?](/mitmachen)
* [geplante und bereits realisierte Dienste](/dienste)
* [Anleitungen](/anleitungen)
* [Rechtliche Lage in Bezug auf Freifunk Holzwinkel](/rechtslage)

## Informationsmaterial
* [Präsentation zu Freifunk Holzwinkel (~ 5 Minuten)](/präsentation-11.03.2019)
* [Präsentation für den Linux-Infotag an der Hochschule Augsburg (~ 7 Minuten)](/präsentation-06.04.2019) und als [PDF](/präsentation-06.04.2019/präsentation.pdf)
* Plakat Infoabend
<br>
<a href="/plakat-fhd.png" target="_blank"><img style="width: 200px;" src="/plakat-fhd.png" alt="Plakat Infoabend"></a>
* Flyer Infoabend:
<br>
<a href="/flyer-vorne-fhd.png" target="_blank"><img style="width: 200px;" src="/flyer-vorne-fhd.png" alt="Plakat Infoabend"></a>
<a href="/flyer-hinten-fhd.png" target="_blank"><img style="width: 200px;" src="/flyer-hinten-fhd.png" alt="Plakat Infoabend"></a>
