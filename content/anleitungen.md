---
title: "Anleitungen"
date: 2018-10-29T20:31:50+06:00
author: Mueller
tags: [Anleitungen,Antennen,Gehäuse]
---

* [Router aufstellen](/router-aufstellen)
* [Outdoorfähigen Router basteln (im Freifunk-Wiki)](https://wiki.freifunk.net/Outdoorf%C3%A4higen_Router_basteln)
