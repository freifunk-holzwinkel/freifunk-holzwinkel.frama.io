---
title: "Mitmachen"
date: 2018-08-31T20:35:47+06:00
author:
tags: []
---
# Wie kann ich mich an Freifunk Holzwinkel beteiligen?

* Router aufstellen
* anderen Leuten von Freifunk Holzwinkel erzählen
* lokale Dienste betreiben
* Firmware und andere Softwares weiterentwickeln und verbessern
* zu den regelmäßigen Freifunk-Treffen kommen
* Texte für die Webseite, Flyer und ähnliches schreiben
* Infomaterial wie Flyer, Plakate und Aufkleber erstellen und verbessern
* Webseite und soziale Medien pflegen
* [Antennen bauen und Router outdoorfähig machen](../anleitungen)

---

Nichts passendes dabei? [Kontaktiere uns](/kontakt)!
