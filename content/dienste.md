---
title: "Dienste"
date: 2018-08-31T20:35:47+06:00
author:
tags: []
---
# Dienste im lokalen Freifunk-Netz

Folgende Dienste können im Freifunk Holzwinkel-Netz genutzt werden, sind aber nicht aus dem Internet erreichbar:
<div target="_blank">
<!--* [Radio Holzwinkel](ipfs://radio.ffhw) (noch nicht in Betrieb)
* [Wetterstation](ipfs://wetter.ffhw) (noch nicht in Betrieb)
* [IPFS](ipfs://ipfs.ffhw) (noch nicht in Betrieb)-->

* [Kiwix](http://10.116.240.240:1111): Lokale Wikipedia-Kopie und viele weitere Offline-Inhalte
* ["Digitale Bibliothek"](/inhalte): Verschiedenste freie Inhalte (Bücher, Zeitschriften, Filme, Podcasts, Musik, ...) zum Stöbern, anschauen, herunterladen und weitergeben; **diese Inhalte werden legal weitergegeben, da der Ersteller dies ausdrücklich erlaubt hat**
* [Thermometer](http://10.116.240.241): Zeigt die aktuelle Temperatur
<!--* [Radiolise](): Höre deine Lieblingsradiosender
<!-- * [Portaal](): Alternatives Suchportal -->
<!-- * [Mastodon](): Dezentrale Twitter-Alternative -->
<!-- * [Etherpad](): Gemeinsam in Echtzeit Texte schreiben-->
<!-- * [Freifunk Holzwinkel-Webseite]() -->
<!-- * [Radio](): Simples Online-Radio -->
<!-- * [Tonwsourced](): Das schwarze Brett fürs Freifunk-Netz -->

## Dienste ohne zentralen Server
* [Scuttlebutt](https://www.scuttlebutt.nz/): Verteilte Facebook-Alternative ohne zentrale Server
* [Manyverse](https://www.manyver.se/): Android-App für Scuttlebutt
* [KouChat](https://www.kouchat.net/): Lokaler Chat-Messenger
<!-- * [Syncthing](): Synchronisiere Deine Geräte -->
</div>
