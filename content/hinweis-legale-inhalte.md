---
title: "Hinweis"
date: 2018-01-26T00:21:35+06:00
author:
tags: []
---
# Nur legale Inhalte

Bei den Medien in der angebotenen digitalen Bibliothek handelt es sich **nicht** um **Raubkopien** oder illegal weiterverbreitete Inhalte.

Alle **Medien** in der Online-Bibliothek stehen unter einer **freien Lizenz** (SIEHE WIKIPEDIA!!! oder CREATIVE COMMONS). Das erlaubt es uns, diese Inhalte zum Download anzubieten. [Wie, da verschenkt jemand seine Bücher?](CREATIVE COMMONS HINTERGRUND / MOTIVATION)

# [Weiter zur digitalen Bibliothek](10.11.160.200)
