---
title: "Kontakt"
date: 2018-10-21T17:25:00+06:00
author:
tags: []
---
# Chat

WhatsApp-Gruppe: https://chat.whatsapp.com/invite/GwbssyJJz214HjgqHQb3T6

Matrix-Gruppe: [#freifunk-holzwinkel.tchncs.de](https://matrix.to/#/#freifunk-holzwinkel:tchncs.de), auch direkt erreichbar mit Riot unter https://riot.im/app/#/room/#freifunk-holzwinkel:tchncs.de

Der Austausch zu Freifunk Holzwinkel findet hauptäschlich in den Chat-Gruppen von Freifunk Holzwinkel statt.
<br>
<br>
Es wurde in Erwägung gezogen, die WhatsApp-Gruppe und die Matrix-Gruppe zu bridgen, also alle Nachrichten automatisch in die jeweils andere Gruppe weiterleiten zu lassen. Leider funktioniert die Implementierung der Bridge auf WhatsApp-Seite nicht (in ausreichendem Maße).
<br>

# E-Mail

<script>
function showEmailAddress(){
if(confirm("Wollen Sie die E-Mail-Adresse wirklich sehen?")){
alert(shiftString("\ufff7\u0003\ufff6\ufffa\ufff7\u0006\uffff\ufffcﾾ\ufff9\u0000�\u000b\u0008\ufffa\uffff\ufffc\ufff6�\u000c￬Uￖ￥￥￥￮\u000e\u0004\u0000�\ufffa\ufff5\ufff2\u0003\ufffa\u0004﾿\ufffe\ufff6", 111));
}
}

function shiftString(text, distance){
    var shiftedText="";
    for(var i=0; i<text.length; i++){
        shiftedText+=String.fromCharCode(text.charCodeAt(i)+distance);
    }
    return shiftedText;
}
</script>

Für privatere Anfragen kann die allgemeine E-Mail-Adresse von Freifunk Holzwinkel genutzt werden: [E-Mail-Adresse anzeigen](javascript: showEmailAddress())
