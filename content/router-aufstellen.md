---
title: "Router Aufstellen"
date: 2019-02-20T22:07:45+01:00
draft: false
tags: []
---

# Freifunk-Router aufstellen

**Wir helfen Dir gerne dabei, einen oder mehrere Freifunk-Router aufzustellen. Auf der [Kontakt-Seite](/kontakt) wird beschrieben, wie Du uns erreichen kannst.**

## Kompatiblen Router kaufen

Nicht jeder Router ist mit unserer Freifunk-Firmware kompatibel.

Alle unterstützten Router findest Du auf der [Firmware-Seite von Freifunk Kiel](https://freifunk.in-kiel.de/firmware.html)

**Achtung:** Von vielen Routern gibt es unterschiedliche Versionen. Wenn Du unsicher bist, welche die richtige ist, kannst Du uns [kontaktieren](/kontakt).

Empfohlene Router:

* TP-Link TL-WR1043 V5 (~ 40 €) (wird leider nicht mehr produziert)
* FRITZ!Box 4020 (~ 45 €)
* TP-Link Archer C7 (~ 70 €)

## Firmware installieren

Im Moment hat Freifunk Holzwinkel noch keine eigene Firmware.

Zu Testzwecken und bis wir eine eigene Firmware haben, verwenden wir die Firmware von [Freifunk Kiel](https://freifunk.in-kiel.de) mit.

Die Firmware von Freifunk Kiel kannst Du auf der [Firmware-Seite](https://freifunk.in-kiel.de/firmware.html) herunterladen. Wenn du Deinen Router das erste Mal installierst, wähle "Erstinstallation" aus.

Bei den meisten Routern kann über die Weboberfläche auf der Update-Seite die Firmware-Datei hochgeladen werden. Diese wird dann ohne weiteres Zutun installiert und der Router startet automatisch neu.

Bei einigen neueren Modellen ist die Installation komplizierter. Erkundige Dich hierfür bitte im Wiki von [OpenWRT](https://openwrt.org) (<- darauf basiert die Freifunk-Firmware) oder recherchiere bei anderen Freifunk-Communities nach Installationsanleitungen.

<!--Bei einigen neueren Modellen wird es komplizierter. Dort muss der Router in eine Art Reset-Modus versetzt werden, in dem er sich von einem mit ihm verbundenen TFTP-Server mit einer bestimmten IP-Adresse die Firmware-Datei mit einem bestimmten Namen herunterlädt und installiert.-->

## Konfigurieren

Beim ersten Anschalten startet der Freifunk-Router automatisch in den Konfigurationsmodus. Verbinde Deinen Computer mit einem der freien LAN-Ports (nicht mit dem einzelnen andersfarbigen WAN-Port). Die Konfigurationsseite erreichst du unter <a href="http://10.116.254.254" target="_blank">10.116.254.254</a>.

Trage dort alle nötigen Informationen wie Routerstandort und E-Mail-Adresse ein und starte den Router neu.

## Aufstellen

Stelle Deinen Freifunk-Router möglichst in Fensternähe auf, da dicke Betonwände das WLAN-Signal stark dämpfen.

Mit der Ausrichtung der Antennen kann zusätzlich die Senderichtung beeinflusst werden.

## Anschließen

Verbinde den WAN-Port des Freifunk-Routers mithilfe des mitgelieferten LAN-Kabels mit einem freien LAN-Port an Deinem privaten WLAN-Router.

<!--Wenn Du die Option Mesh On WAN nicht aktiviert hast, kann man vom Freifunk-Netz aus nicht auf Dein Heimnetzwerk zugreifen.-->

Mit den Standardeinstellungen kann man vom Freifunk-Netz aus nicht auf Dein privates Heimnetzwerk zugreifen.

Es wird empfohlen, dass Du auf Deinem privaten WLAN-Router ein Gastnetzwerk für die LAN-Buchse, an die Dein Freifunk-Router angeschlossen ist, einrichtest. Damit ist ein Heimnetzwerk zusätzlich abgeschottet und Du kannst eventuell einstellen, dass nur die ungenutzte Bandbreite Deines Internetanschlusses über Freifunk freigegeben wird.

Unter Umständen musst Du an Deinem privaten Router die Filtereinstellungen so abändern, dass der Freifunk-Router das Gateway erreicht.

---

Noch nicht genug? Erfahre andere Wege, wie Du bei Freifunk Holzwinkel [mitmachen](/mitmachen) kannst!
