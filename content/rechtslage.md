---
title: "Rechtliche Lage"
date: 2018-10-29T20:31:50+06:00
author:
tags: []
---
# Rechtliche Grundlagen der technischen Umsetzung der Freifunk-Netze

**WICHTIG: Dieser Text ist lediglich eine Zusammenfassung der Informationen aus den aufgeführten Quellen. Der Inhalt kann falsch, unvollständig und veraltet sein. Es wird keinerlei Haftung übernommen. Stand: 29. November 2018.**

## Frequenzbereiche

Die Bundesnetzagentur hat die Frequenzbereiche 2400,0 MHz – 2483,5 MHz (2,4 GHz) [1] sowie 5150 MHz - 5350 MHz und 5470 MHz - 5725 MHz (5 GHz) [2] für die Nutzung durch die Allgemeinheit für WLAN und andere Anwendungen zugeteilt. [3] Das heißt, dass jede Person unter Einhaltung der maximal zulässigen Strahlungsleistungen innerhalb dieser Frequenzbereiche und eventuell unter Berücksichtugung weiterer Bestimmungen (z.B. Leistungsregelungen, Minderungstechniken) WLAN-Zugangspunkte betreiben darf, ohne dass diese eine separate Genehmigung benötigt. Die handelsüblichen WLAN-Router, welche für Freifunk verwendet werden, funken innerhalb dieser Frequenzbereiche und sind normalerweise so eingestellt, dass die maximal zulässige Sendeleistung nicht überschritten wird.

## Störerhaftung

Aufgrund der WLAN-Störerhaftung, welche im Telemediengesetz [4] festgelegt war, konnten Betreiber öffentlich zugänglicher WLAN-Zugangspunkte für Urheberrechtsverstöße, welche über ihren Zugang begangen wurden, haftbar gemacht werden. Dies hatte viele kostenpflichtige Unterlassungsforderungen und Abmahnungen zur Folge. Die Regelung galt nicht für Internetdienstanbieter (kurz ISP von Internet Service Provider), denn diese hatten in diesem Punkt das sogenannte Providerprivileg. [5]

Um in dieser Sache nicht haften zu müssen, leiten viele Freifunker den Internetverkehr ihrer Router über VPN-Server um, die entweder im Ausland stehen oder von einem Freifunk-Verein, welcher als ISP eingetragen ist, betrieben werden. [6]

Voriges Jahr wurde zum Ende der Legislaturperiode des vorhergehenden Bundestages am 30. Juni 2017 mit der dritten Überarbeitung des Telemediengesetzes die WLAN-Störerhaftung offiziell abgeschafft. [7] Diese Änderung löste das Problem aber nicht komplett, denn gleichzeitig wurde eine Regelung zu möglichen Netzsperren eingeführt. Diese besagt, dass Urheberrechtsinhaber die Möglichkeit haben, die Betreiber von öffentlich zugänglichen WLAN-Zugangspunkten zu verpflichten, Seiten und Inhalte zu sperren, die im Zusammenhang mit der beanstandeten Urheberrechtsverletzung stehen. Dieses Mittel darf aber nur als letzte Möglichkeit genutzt werden, wenn alle anderen Wege, gegen die Urheberrechtsverletzung vorzugehen, erfolglos waren. Außerdem müssen die Kosten für solche Netzsperren von den Rechteinhabern getragen werden. [8] Die Abschaffung der Störerhaftung wurde bereits durch Gerichtsurteile vom Oberlandesgericht München und vom Bundesgerichtshof bestätigt. [9] [10]

## Lösungsansätze

Für Freifunk Holzwinkel gibt es bezüglich der Regelungen im aktuellen Telemediengesetz generell zwei Möglichkeiten: Entweder man leitet den kompletten Internetverkehr über VPN-Server, entweder eines in Deutschland ansässigen und als ISP eingetragenen Betreibers (zum Beispiel ein Freifunk-Verein) oder über einen im Ausland ansässigen Betreiber, wo die Regelungen des Telemediengesetzes nicht gelten, um. Oder aber man schickt als andere Möglichkeit den Netzwerkverkehr direkt vom Router aus ins Internet und befolgt Sperranweisungen, sofern welche angeordnet werden sollten.

Beide Lösungen haben ihre Vor- und Nachteile. Das Umleiten der Datenpakete über VPN-Server verlangsamt die Verbindung und verursacht zusätzliche Kosten. Netzsperren würden das Freifunk-Netz zensieren, die Kommunikationsfreiheit einschränken, die Netzneutralität verletzen und die bereitgestellte Internetanbindung immer unbenutzbarer machen, je mehr Seiten gesperrt werden.

Eine Kombination aus beiden Möglichkeiten wäre, den Internetverkehr vertrauenswürdiger Webseiten, bei denen die Gefahr einer Sperranforderung aufgrund von Urheberrechtsverletzungen ziemlich gering oder gar nicht vorhanden ist (zum Beispiel, weil auf den betreffenden Seiten keine Inhalte von Dritten veröffentlicht werden können, der Webseitenbetreiber aktiv gegen Urheberrechtsverletzungen auf seiner Plattform vorgeht und / oder der Betreiber einer Webseite vertrauenswürdig ist), direkt ins Internet zu leiten und die restlichen Verbindungen weiterhin über einen VPN-Server zu schicken. Bei dieser Lösung hätte man eine hohe rechtliche Sicherheit sowie eine geringere Datenmenge, die über einen VPN-Server geleitet werden muss, jedoch wäre die Netzneutralität verletzt, da nicht alle Datenpakete gleich behandelt werden würden.

Um den Start von Freifunk Holzwinkel nicht zu erschweren und Mehraufwand zu verhindern, könnte man auch in Erwägung ziehen, für die Anfangsphase nach vorheriger Rücksprache mit den zuständigen Personen die VPN-Server eines anderen Freifunk-Vereins mitzunutzen.

## Teilen des Internetanschlusses

Das Teilen des eigenen Internetanschlusses (zum Beispiel des häuslichen DSL-Anschlusses) ist nicht grundsätzlich, zum Beispiel durch ein Gesetz, verboten. Jedoch kann es sein, dass der ISP, bei dem der Anschluss gebucht ist, das Teilen des Internet-Anschlusses einschränkt oder verbietet. Dies ist von Anbieter zu Anbieter unterschiedlich und muss im Einzelfall geklärt werden. Hält man sich nicht an die Geschäftsbedingungen des Anbieters, kann eine Kündigung des Vertrages drohen. [11] [12]

## Gemeinnützigkeit

Ein weiteres wichtiges Rechts-Thema im Freifunk-Bereich ist die Gemeinnützigkeit von Freifunk-Vereinen. Da diese oftmals als ISP registriert sind, bekommt ein nicht zu vernachlässigender Teil der Freifunk-Vereine von dem entsprechenden Finanzamt die Gemeinnützigkeit nicht anerkannt. [13] [14] Es gibt Bestrebungen, die Gemeinnützigkeit von Freifunk-Vereinen auf Bundesebene gesetzlich zu verankern, aber leider ist ein dementsprechender Gesetzesentwurf aus der letzten Legislaturperiode des Bundestages aufgrund des Diskontinuitäts-Prinzipes verfallen, da das Gesetz nicht mehr vor der Bundestagswahl endgültig beschlossen werden konnte. [15] Union und SPD haben die Anerkennung der Gemeinnützigkeit bei dem Bereitstellen von offenen WLAN-Netzen in ihren Koalitionsvertrag mit aufgenommen (Zeile 1702ff) und in diesem Punkt explizit Freifunk genannt. [16] Am 23. November 2018 hat der Bundesrat einen Gesetzesentwurf zur Anerkennung der Gemeinützigkeit von Freifunk beschlossen. [15] [17] Deswegen kann man hoffen, dass Freifunk auf Bundesebene als gemeinnützig anerkannt werden wird.

## Fazit

Allgemein kann nach der derzeitigen rechtlichen Lage gesagt werden, dass es in Deutschland keine rechtlichen Bestimmungen gibt, welche das Betreiben von Freifunk-Knoten und Freifunk im Allgemeinen verbieten / unmöglich machen und dadurch auch die Unterstützung durch und Zusammenarbeit mit Privatpersonen, Vereinen, Geschäften und Kommunen verwehren würden. Die noch verbliebenen Hürden, zum Beispiel die durch das Telemediengesetz drohende Sperrpflicht, sind mit vertretbarem Aufwand zu meistern.

Dass das Betreiben von Freifunk-Routern bei der aktuellen rechtlichen Lage möglich ist, zeigt unter anderem die Verbreitung von Freifunk: In Deutschland gibt es mindestens 44.601 Freifunk-Knoten in 378 lokalen Freifunk-Gruppen (Stand: 29. November 2018). Am 3. Februar 2019 war die Knotenanzahl schon auf mindestens 45.443 und die Anzahl der lokalen Freifunk-Gruppen auf mindestens 387 angewachsen. [18]

## Quellenverzeichnis:

[1] https://www.bundesnetzagentur.de/SharedDocs/Downloads/DE/Sachgebiete/Telekommunikation/Unternehmen_Institutionen/Frequenzen/Allgemeinzuteilungen/2013_10_WLAN_2,4GHz_pdf.pdf;jsessionid=53FC822B06B303850CBBD9C6AD54EAE2?__blob=publicationFile&v=5

[2] https://www.bundesnetzagentur.de/SharedDocs/Downloads/DE/Sachgebiete/Telekommunikation/Unternehmen_Institutionen/Frequenzen/Allgemeinzuteilungen/2010_07_WLAN_5GHz_pdf.pdf;jsessionid=53FC822B06B303850CBBD9C6AD54EAE2?__blob=publicationFile&v=5

[3] https://www.bundesnetzagentur.de/DE/Sachgebiete/Telekommunikation/Unternehmen_Institutionen/Frequenzen/OeffentlicheNetze/RegionaleNetze/regionalenetze-node.html

[4] https://www.bundesrat.de/SharedDocs/drucksachen/2006/0501-0600/556-06.pdf?__blob=publicationFile&v=1

[5] https://netzpolitik.org/2015/netzrueckblick-die-stoererhaftung-eine-einfuehrung/

[6] https://www.lz.de/lippe/kreis_lippe/20803063_WLAN-Hotspots-sollen-in-Zukunft-deutlich-schneller-werden.html

[7] http://dip21.bundestag.de/dip21/btd/18/122/1812202.pdf

[8] https://netzpolitik.org/2017/wlan-gesetz-bundestag-schafft-stoererhaftung-endlich-ab-ermoeglicht-aber-netzsperren/

[9] https://netzpolitik.org/2018/gericht-bestaetigt-abschaffung-der-wlan-stoererhaftung/

[10] https://netzpolitik.org/2018/bundesgerichtshof-bestaetigt-abschaffung-der-stoererhaftung-aber-auch-netzsperren/

[11] https://irights.info/artikel/ein-netz-voller-fallgruben-stoererhaftung-datenschutz-meldepflicht-faq/24641

[12] https://www.teltarif.de/internet-mit-nachbar-teilen/news/71236.html

[13] https://freifunk.net/blog/2018/11/gemeinnuetzigkeit-fuer-freifunk/

[14] https://radio.freifunk.net/2018/02/14/ffradio058-digitales-ehrenamt-und-gemeinnuetzigkeit/

[15] https://netzpolitik.org/2018/bundesrat-stimmt-erneut-fuer-gemeinnuetzigkeit-von-freifunk-initiativen/

[16] https://www.bundesregierung.de/resource/blob/975226/847984/5b8bc23590d4cb2892b31c987ad672b7/2018-03-14-koalitionsvertrag-data.pdf?download=1

[17] https://www.bundesrat.de/SharedDocs/drucksachen/2018/0501-0600/573-18.pdf?__blob=publicationFile&v=1

[18] https://www.freifunk-karte.de/
